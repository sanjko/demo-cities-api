package hr.sanjin.citiesapi.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Value("${cts.token.header.name}")
	private String tokenHeaderName;

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2) //  
				.select() //                                  
				.apis(RequestHandlerSelectors.basePackage("hr.sanjin.citiesapi.controller")) //              
				.paths(PathSelectors.any()) //				
				.build().securitySchemes(Arrays.asList(apiKey()));
	}

	@Bean
	public SecurityConfiguration securityInfo() {
		return new SecurityConfiguration(null, null, null, null, "", ApiKeyVehicle.HEADER, this.tokenHeaderName, "");
	}

	private ApiKey apiKey() {
		return new ApiKey(this.tokenHeaderName, this.tokenHeaderName, "header");
	}

}
