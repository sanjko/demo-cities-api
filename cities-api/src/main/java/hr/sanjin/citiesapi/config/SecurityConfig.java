package hr.sanjin.citiesapi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import hr.sanjin.citiesapi.entity.User;
import hr.sanjin.citiesapi.service.UserService;

@Configuration
@EnableTransactionManagement
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${cts.token.header.name}")
	private String tokenHeaderName;

	@Autowired
	private UserService userService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		PreAuthTokenHeaderFilter preAuthFilter = createPreAuthFilter();

		//@formatter:off
			http
				.csrf().disable()		
				//.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.addFilter(preAuthFilter)
				.addFilterBefore(new ExceptionTranslationFilter(
	                    new Http403ForbiddenEntryPoint()),
						preAuthFilter.getClass()
	                )
				.authorizeRequests()
					.antMatchers("/api/public/**",		
							"/webjars/**",
							"/v2/api-docs", "/configuration/ui", "/swagger-resources/**", "/configuration/security", 
							"/swagger-ui.html").permitAll()
				.anyRequest().authenticated();
					
				
		//@formatter:on
	}

	private PreAuthTokenHeaderFilter createPreAuthFilter() {
		PreAuthTokenHeaderFilter filter = new PreAuthTokenHeaderFilter(this.tokenHeaderName);

		filter.setAuthenticationManager(authentication -> {
			String principal = (String) authentication.getPrincipal();

			User user = this.userService.getUserByToken(principal).orElseThrow(() -> new BadCredentialsException("The token does not exist."));

			if (this.userService.hasTokenExpired(user)) {
				throw new BadCredentialsException("Token expired.");
			}

			authentication.setAuthenticated(true);
			return authentication;
		});

		return filter;
	}

	@Bean
	@Profile("!integration-test")
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Profile("integration-test")
	public PasswordEncoder passwordEncoderForTest() {
		return NoOpPasswordEncoder.getInstance();
	}
}
