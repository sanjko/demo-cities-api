package hr.sanjin.citiesapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class RegistrationFailedException extends RuntimeException {

	private static final long serialVersionUID = -6990613408506309416L;

	public RegistrationFailedException(String message) {
		super(message);
	}

	public RegistrationFailedException(String message, Throwable cause) {
		super(message, cause);
	}
}
