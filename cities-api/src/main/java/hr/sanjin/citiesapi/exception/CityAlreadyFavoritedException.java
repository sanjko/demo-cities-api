package hr.sanjin.citiesapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CityAlreadyFavoritedException extends RuntimeException {

	private static final long serialVersionUID = -6990613408506309416L;

	public CityAlreadyFavoritedException(String message) {
		super(message);
	}

	public CityAlreadyFavoritedException(String message, Throwable cause) {
		super(message, cause);
	}
}
