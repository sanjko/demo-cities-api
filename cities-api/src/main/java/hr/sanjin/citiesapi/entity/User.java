package hr.sanjin.citiesapi.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "token")
	private String token;

	@Column(name = "tokenCreated")
	private ZonedDateTime tokenCreationDate;

	@ManyToMany(fetch = FetchType.LAZY, //
			cascade = { //
					CascadeType.PERSIST, //
					CascadeType.MERGE //
			})
	@JoinTable(name = "user_cities", //
			joinColumns = @JoinColumn(name = "user"), //
			inverseJoinColumns = @JoinColumn(name = "city") //
	)
	private Set<City> cities = new HashSet<>();

	public void addCity(City city) {
		this.cities.add(city);
		city.getUsers().add(this);
	}

	public void removeCity(City city) {
		this.cities.remove(city);
		city.getUsers().remove(this);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public ZonedDateTime getTokenCreationDate() {
		return this.tokenCreationDate;
	}

	public void setTokenCreationDate(ZonedDateTime tokenCreationDate) {
		this.tokenCreationDate = tokenCreationDate;
	}

	public Set<City> getCities() {
		return this.cities;
	}

	public void setCities(Set<City> cities) {
		this.cities = cities;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		User other = (User) obj;
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		return true;
	}

}
