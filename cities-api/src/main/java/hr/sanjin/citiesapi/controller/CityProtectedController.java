package hr.sanjin.citiesapi.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import hr.sanjin.citiesapi.controller.request.CreateCityRequest;
import hr.sanjin.citiesapi.controller.request.FavoriteCityRequest;
import hr.sanjin.citiesapi.entity.User;
import hr.sanjin.citiesapi.service.AuthService;
import hr.sanjin.citiesapi.service.CityService;
import hr.sanjin.citiesapi.service.dto.CityDTO;

@RestController
@RequestMapping(value = "/api/cities", produces = MediaType.APPLICATION_JSON_VALUE)
public class CityProtectedController {

	@Autowired
	private CityService cityService;

	@Autowired
	private AuthService authService;

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public CityDTO createCity(@Valid @RequestBody CreateCityRequest createCityRequest) {
		return this.cityService.createCity(createCityRequest);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/favorite")
	@ResponseStatus(HttpStatus.CREATED)
	public void addFavoriteCity(@Valid @RequestBody FavoriteCityRequest addFavoriteCityRequest) {
		User user = this.authService.getLoggedUserWithRelations();
		this.cityService.addFavoriteCity(user, addFavoriteCityRequest);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/favorite")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removeFavoriteCity(@Valid @RequestBody FavoriteCityRequest removeFavoriteCityRequest) {
		User user = this.authService.getLoggedUserWithRelations();
		this.cityService.removeFavoriteCity(user, removeFavoriteCityRequest);
	}

}
