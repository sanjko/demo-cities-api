package hr.sanjin.citiesapi.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import hr.sanjin.citiesapi.controller.request.LoginUserRequest;
import hr.sanjin.citiesapi.controller.request.RegisterUserRequest;
import hr.sanjin.citiesapi.service.UserService;
import hr.sanjin.citiesapi.service.dto.LoginTokenDTO;
import hr.sanjin.citiesapi.service.dto.UserDTO;

@RestController
@RequestMapping(value = "/api/public/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.POST, value = "/register")
	@ResponseStatus(HttpStatus.CREATED)
	public UserDTO registerUser(@Valid @RequestBody RegisterUserRequest registerUserRequest) {
		UserDTO createdUser = this.userService.registerUser(registerUserRequest);
		return createdUser;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/login")
	public LoginTokenDTO loginUser(@Valid @RequestBody LoginUserRequest loginUserRequest) {
		return this.userService.loginUser(loginUserRequest);
	}
}
