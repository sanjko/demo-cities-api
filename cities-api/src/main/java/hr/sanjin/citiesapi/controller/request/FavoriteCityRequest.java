package hr.sanjin.citiesapi.controller.request;

import javax.validation.constraints.NotNull;

public class FavoriteCityRequest {

	@NotNull(message = "CityId is mandatory")
	private Integer cityId;

	public Integer getCityId() {
		return this.cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

}
