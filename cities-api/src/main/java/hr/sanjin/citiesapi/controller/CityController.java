package hr.sanjin.citiesapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hr.sanjin.citiesapi.service.CityService;
import hr.sanjin.citiesapi.service.dto.CityDTO;
import hr.sanjin.citiesapi.service.dto.CityWithCountDTO;

@RestController
@RequestMapping(value = "/api/public/cities", produces = MediaType.APPLICATION_JSON_VALUE)
public class CityController {

	@Autowired
	private CityService cityService;

	@RequestMapping(method = RequestMethod.GET)
	public List<CityDTO> getAllCities() {
		return this.cityService.getAllCities();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/date")
	public List<CityDTO> getAllCitiesByDate() {
		return this.cityService.getAllCitiesByDate();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/favorites")
	public List<CityWithCountDTO> getAllCitiesByFavorites() {
		return this.cityService.getAllCitiesByFavorites();
	}
}
