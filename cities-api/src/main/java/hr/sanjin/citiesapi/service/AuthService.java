package hr.sanjin.citiesapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import hr.sanjin.citiesapi.entity.User;

@Service
public class AuthService {

	@Autowired
	private UserService userService;

	public User getLoggedUser() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof String) {
			return this.userService.getUserByToken(principal.toString()).orElseThrow(() -> new BadCredentialsException("Invalid token"));
		}

		return null;
	}

	public User getLoggedUserWithRelations() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof String) {
			return this.userService.getUserByTokenWithRelations(principal.toString()).orElseThrow(() -> new BadCredentialsException("Invalid token"));
		}

		return null;
	}

}
