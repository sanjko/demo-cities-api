package hr.sanjin.citiesapi.service.dto;

import hr.sanjin.citiesapi.entity.User;

public class UserDTO {

	private Integer id;
	private String username;

	public static UserDTO fromEntity(User entity) {
		UserDTO dto = new UserDTO();
		dto.setId(entity.getId());
		dto.setUsername(entity.getUsername());

		return dto;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
