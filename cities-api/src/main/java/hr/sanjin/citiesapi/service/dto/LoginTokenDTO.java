package hr.sanjin.citiesapi.service.dto;

public class LoginTokenDTO {

	private String token;

	public LoginTokenDTO() {
	}

	public LoginTokenDTO(String token) {
		this.token = token;
	}

	public String getToken() {
		return this.token;
	}

}
