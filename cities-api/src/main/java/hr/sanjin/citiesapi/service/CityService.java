package hr.sanjin.citiesapi.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.sanjin.citiesapi.controller.request.CreateCityRequest;
import hr.sanjin.citiesapi.controller.request.FavoriteCityRequest;
import hr.sanjin.citiesapi.entity.City;
import hr.sanjin.citiesapi.entity.User;
import hr.sanjin.citiesapi.exception.CityAlreadyFavoritedException;
import hr.sanjin.citiesapi.exception.CityNotFavoritedException;
import hr.sanjin.citiesapi.exception.EntityNotFoundException;
import hr.sanjin.citiesapi.repository.CityRepository;
import hr.sanjin.citiesapi.repository.UserRepository;
import hr.sanjin.citiesapi.service.dto.CityDTO;
import hr.sanjin.citiesapi.service.dto.CityWithCountDTO;
import hr.sanjin.citiesapi.util.DateTimeUtil;

@Service
public class CityService {

	@Autowired
	private CityRepository cityRepo;

	@Autowired
	private UserRepository userRepo;

	public List<CityDTO> getAllCities() {
		return this.cityRepo.findAll().stream() //
				.map(CityDTO::fromEntity) //
				.collect(Collectors.toList());
	}

	public List<CityDTO> getAllCitiesByDate() {
		return this.cityRepo.findAllByOrderByCreated().stream() //
				.map(CityDTO::fromEntity) //
				.collect(Collectors.toList());
	}

	public CityDTO createCity(CreateCityRequest createCityRequest) {
		City city = new City();
		city.setName(createCityRequest.getName());
		city.setDescription(createCityRequest.getDescription());
		city.setPopulation(createCityRequest.getPopulation());
		city.setCreated(DateTimeUtil.now());

		City savedCity = this.cityRepo.save(city);

		return CityDTO.fromEntity(savedCity);
	}

	@Transactional
	public void addFavoriteCity(User user, FavoriteCityRequest addFavoriteCityRequest) {
		City city = this.cityRepo.findById(addFavoriteCityRequest.getCityId()).orElseThrow(() -> new EntityNotFoundException("City does not exist"));

		if (this.cityRepo.existsByIdAndUsers_Id(city.getId(), user.getId())) {
			throw new CityAlreadyFavoritedException("User already added this city to favorites");
		}

		user.addCity(city);

		this.userRepo.save(user);
	}

	@Transactional
	public void removeFavoriteCity(User user, FavoriteCityRequest removeFavoriteCityRequest) {
		City city = this.cityRepo.findById(removeFavoriteCityRequest.getCityId()).orElseThrow(() -> new EntityNotFoundException("City does not exist"));

		if (!this.cityRepo.existsByIdAndUsers_Id(city.getId(), user.getId())) {
			throw new CityNotFavoritedException("User did not add this city to favorites");
		}

		user.removeCity(city);

		this.userRepo.save(user);

	}

	public List<CityWithCountDTO> getAllCitiesByFavorites() {
		return this.cityRepo.findAllByOrderByFavorites().stream() //
				.map(CityWithCountDTO::fromEntity) //
				.collect(Collectors.toList());
	}

}
