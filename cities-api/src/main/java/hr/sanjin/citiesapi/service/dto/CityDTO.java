package hr.sanjin.citiesapi.service.dto;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import hr.sanjin.citiesapi.entity.City;
import hr.sanjin.citiesapi.util.DateTimeUtil;

public class CityDTO {

	private Integer id;
	private String name;
	private String description;
	private int population;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtil.DATE_TIME_FORMAT, timezone = DateTimeUtil.DATE_TIME_FORMAT_ZONE_API)
	private ZonedDateTime created;

	public static CityDTO fromEntity(City city) {
		CityDTO dto = new CityDTO();
		dto.setId(city.getId());
		dto.setName(city.getName());
		dto.setDescription(city.getDescription());
		dto.setPopulation(city.getPopulation());
		dto.setCreated(city.getCreated());
		return dto;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPopulation() {
		return this.population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

	public ZonedDateTime getCreated() {
		return this.created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

}
