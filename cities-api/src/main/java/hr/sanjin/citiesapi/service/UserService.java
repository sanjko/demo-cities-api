package hr.sanjin.citiesapi.service;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import hr.sanjin.citiesapi.controller.request.LoginUserRequest;
import hr.sanjin.citiesapi.controller.request.RegisterUserRequest;
import hr.sanjin.citiesapi.entity.User;
import hr.sanjin.citiesapi.exception.LoginFailedException;
import hr.sanjin.citiesapi.repository.UserRepository;
import hr.sanjin.citiesapi.service.dto.LoginTokenDTO;
import hr.sanjin.citiesapi.service.dto.UserDTO;
import hr.sanjin.citiesapi.util.DateTimeUtil;

@Service
public class UserService {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepo;

	@Value("${cts.token.validity.minutes:60}")
	private long tokenValidityInMinutes;

	public UserDTO registerUser(RegisterUserRequest registerUserRequest) {
		checkIfUserExists(registerUserRequest);

		User newUser = new User();
		newUser.setUsername(registerUserRequest.getUsername());
		newUser.setPassword(this.passwordEncoder.encode(registerUserRequest.getPassword()));
		newUser.setToken(UUID.randomUUID().toString());
		newUser.setTokenCreationDate(DateTimeUtil.now());

		User savedUser = this.userRepo.save(newUser);
		return UserDTO.fromEntity(savedUser);
	}

	@Transactional
	public LoginTokenDTO loginUser(LoginUserRequest loginUserRequest) {
		User user = this.userRepo.findByUsername(loginUserRequest.getUsername()).orElseThrow(() -> new LoginFailedException("Login failed!"));

		validatePassword(user, loginUserRequest.getPassword());

		return getUserToken(user);
	}

	private LoginTokenDTO getUserToken(User user) {
		String token = user.getToken();

		if (tokenExpired(user.getTokenCreationDate())) {
			token = generateAndPersistToken(user);
		}

		return new LoginTokenDTO(token);
	}

	private String generateAndPersistToken(User user) {
		user.setToken(UUID.randomUUID().toString());
		user.setTokenCreationDate(DateTimeUtil.now());

		this.userRepo.save(user);

		return user.getToken();
	}

	private boolean tokenExpired(ZonedDateTime tokenCreationDate) {
		return tokenCreationDate.plus(this.tokenValidityInMinutes, ChronoUnit.MINUTES).isBefore(DateTimeUtil.now());
	}

	private void validatePassword(User user, String password) {
		boolean passwordOK = this.passwordEncoder.matches(password, user.getPassword());

		if (!passwordOK) {
			throw new LoginFailedException("Login failed!");
		}
	}

	public Optional<User> getUserByToken(String principal) {
		return this.userRepo.findByToken(principal);
	}

	public Optional<User> getUserByTokenWithRelations(String principal) {
		return this.userRepo.findWithRelationsByToken(principal);
	}

	public boolean hasTokenExpired(User user) {
		return tokenExpired(user.getTokenCreationDate());
	}

	private void checkIfUserExists(RegisterUserRequest request) {
		if (this.userRepo.findByUsername(request.getUsername()).isPresent()) {
			throw new LoginFailedException("User already exists");
		}

	}

}
