package hr.sanjin.citiesapi.service.dto;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import hr.sanjin.citiesapi.repository.CityRepository.CityWithCount;
import hr.sanjin.citiesapi.util.DateTimeUtil;

public class CityWithCountDTO extends CityDTO {

	private Integer id;
	private String name;
	private String description;
	private int population;
	private int usersFavorited;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtil.DATE_TIME_FORMAT, timezone = DateTimeUtil.DATE_TIME_FORMAT_ZONE_API)
	private ZonedDateTime created;

	public static CityWithCountDTO fromEntity(CityWithCount city) {
		CityWithCountDTO dto = new CityWithCountDTO();
		dto.setId(city.getId());
		dto.setName(city.getName());
		dto.setDescription(city.getDescription());
		dto.setPopulation(city.getPopulation());
		dto.setCreated(city.getCreated());
		dto.setUsersFavorited(city.getUsersCount());
		return dto;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int getPopulation() {
		return this.population;
	}

	@Override
	public void setPopulation(int population) {
		this.population = population;
	}

	@Override
	public ZonedDateTime getCreated() {
		return this.created;
	}

	@Override
	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public int getUsersFavorited() {
		return this.usersFavorited;
	}

	public void setUsersFavorited(int usersFavorited) {
		this.usersFavorited = usersFavorited;
	}

}
