package hr.sanjin.citiesapi.util;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DateTimeUtil {

	public static final String DATE_TIME_FORMAT = "dd-MM-yyyy-HH:mm:ssZ";

	public static final String DATE_TIME_FORMAT_ZONE_API = "UTC";

	public static final ZoneId ZONE_ID_API = ZoneId.of(DATE_TIME_FORMAT_ZONE_API);

	public static ZonedDateTime now() {
		return ZonedDateTime.now(ZONE_ID_API);
	}

}
