package hr.sanjin.citiesapi.repository;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import hr.sanjin.citiesapi.entity.City;

@Repository
public interface CityRepository extends JpaRepository<City, Integer> {

	List<City> findAllByOrderByCreated();

	@Query("SELECT c.id as id, c.name as name, c.description as description, c.population as population, c.created as created, count(u) as usersCount FROM City c "//
			+ "left join c.users u "//
			+ "group by c " //
			+ "order by usersCount desc ")
	List<CityWithCount> findAllByOrderByFavorites();

	boolean existsByIdAndUsers_Id(int cityId, int userId);

	public interface CityWithCount {
		Integer getId();

		String getName();

		String getDescription();

		Integer getPopulation();

		Integer getUsersCount();

		ZonedDateTime getCreated();

	}

}
