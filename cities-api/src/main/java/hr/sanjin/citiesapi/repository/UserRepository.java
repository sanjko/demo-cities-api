package hr.sanjin.citiesapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.sanjin.citiesapi.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	Optional<User> findByUsername(String username);

	Optional<User> findByToken(String token);

	@EntityGraph(attributePaths = { "cities" })
	Optional<User> findWithRelationsByToken(String token);

}
