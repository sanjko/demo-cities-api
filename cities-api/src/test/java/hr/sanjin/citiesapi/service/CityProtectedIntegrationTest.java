package hr.sanjin.citiesapi.service;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.test.context.jdbc.Sql;

import hr.sanjin.citiesapi.AbstractMockMVCIntegrationTest;
import hr.sanjin.citiesapi.controller.request.CreateCityRequest;
import hr.sanjin.citiesapi.controller.request.FavoriteCityRequest;

@Sql("user_data.sql")
public class CityProtectedIntegrationTest extends AbstractMockMVCIntegrationTest {

	private static final String PATH_CITIES_PROTECTED_CONTROLLER = "/api/cities/";

	private static final String PATH_CREATE_CITY = PATH_CITIES_PROTECTED_CONTROLLER;
	private static final String PATH_FAVORITE = PATH_CITIES_PROTECTED_CONTROLLER + "favorite";

	@Test
	public void createCity_notAuthenticated() throws Exception {
		this.mockMvc //
				.perform(postWithoutAuth(PATH_CREATE_CITY, null)) //
				.andExpect(status().isForbidden());
	}

	@Test
	public void createCity_requestBodyInvalid_noPopulation() throws Exception {
		CreateCityRequest request = new CreateCityRequest();
		request.setName("a name");
		request.setDescription("a desc");

		this.mockMvc //
				.perform(postWithAuth(PATH_CREATE_CITY, request)) //
				.andExpect(status().isBadRequest());
	}

	@Test
	public void createCity_requestBodyInvalid_noName() throws Exception {
		CreateCityRequest request = new CreateCityRequest();
		request.setDescription("a desc");
		request.setPopulation(5);

		this.mockMvc //
				.perform(postWithAuth(PATH_CREATE_CITY, request)) //
				.andExpect(status().isBadRequest());
	}

	@Test
	public void createCity_requestBodyInvalid_noDescription() throws Exception {
		CreateCityRequest request = new CreateCityRequest();
		request.setName("name");
		request.setPopulation(5);

		this.mockMvc //
				.perform(postWithAuth(PATH_CREATE_CITY, request)) //
				.andExpect(status().isBadRequest());
	}

	@Test
	public void createCity_valid() throws Exception {
		CreateCityRequest request = new CreateCityRequest();
		request.setName("name");
		request.setDescription("desc");
		request.setPopulation(5);

		this.mockMvc //
				.perform(postWithAuth(PATH_CREATE_CITY, request)) //
				.andExpect(status().isCreated());
	}

	@Test
	public void addFavoriteCity_notAuthenticated() throws Exception {
		this.mockMvc //
				.perform(postWithoutAuth(PATH_FAVORITE, null)) //
				.andExpect(status().isForbidden());
	}

	@Test
	public void addFavoriteCity_invalidRequest() throws Exception {
		FavoriteCityRequest request = new FavoriteCityRequest();

		this.mockMvc //
				.perform(postWithAuth(PATH_FAVORITE, request)) //
				.andExpect(status().isBadRequest());
	}

	@Test
	public void addFavoriteCity_cityNotFound() throws Exception {
		FavoriteCityRequest request = new FavoriteCityRequest();
		request.setCityId(Integer.MAX_VALUE);

		this.mockMvc //
				.perform(postWithAuth(PATH_FAVORITE, request)) //
				.andExpect(status().isNotFound());
	}

	@Test
	public void addFavoriteCity_valid() throws Exception {
		FavoriteCityRequest request = new FavoriteCityRequest();
		request.setCityId(1);

		this.mockMvc //
				.perform(postWithAuth(PATH_FAVORITE, request)) //
				.andExpect(status().isCreated());
	}

	@Test
	public void addFavoriteCity_duplicateAdd() throws Exception {
		FavoriteCityRequest request = new FavoriteCityRequest();
		request.setCityId(1);

		this.mockMvc //
				.perform(postWithAuth(PATH_FAVORITE, request)) //
				.andExpect(status().isCreated());

		this.mockMvc //
				.perform(postWithAuth(PATH_FAVORITE, request)) //
				.andExpect(status().isBadRequest());
	}

	@Test
	public void removeFavoriteCity_notAuthenticated() throws Exception {
		this.mockMvc //
				.perform(deleteWithoutAuth(PATH_FAVORITE, null)) //
				.andExpect(status().isForbidden());
	}

	@Test
	public void removeFavoriteCity_invalidRequest() throws Exception {
		FavoriteCityRequest request = new FavoriteCityRequest();

		this.mockMvc //
				.perform(deleteWithAuth(PATH_FAVORITE, request)) //
				.andExpect(status().isBadRequest());
	}

	@Test
	public void removeFavoriteCity_cityNotFound() throws Exception {
		FavoriteCityRequest request = new FavoriteCityRequest();
		request.setCityId(Integer.MAX_VALUE);

		this.mockMvc //
				.perform(deleteWithAuth(PATH_FAVORITE, request)) //
				.andExpect(status().isNotFound());
	}

	@Test
	public void removeFavoriteCity_notAdded() throws Exception {
		FavoriteCityRequest request = new FavoriteCityRequest();
		request.setCityId(1);

		this.mockMvc //
				.perform(deleteWithAuth(PATH_FAVORITE, request)) //
				.andExpect(status().isBadRequest());
	}

	@Test
	public void removeFavoriteCity_valid() throws Exception {
		addFavoriteCity_valid();

		FavoriteCityRequest request = new FavoriteCityRequest();
		request.setCityId(1);

		this.mockMvc //
				.perform(deleteWithAuth(PATH_FAVORITE, request)) //
				.andExpect(status().isNoContent());
	}
}
