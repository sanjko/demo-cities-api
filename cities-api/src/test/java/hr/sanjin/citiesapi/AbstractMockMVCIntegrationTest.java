package hr.sanjin.citiesapi;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import hr.sanjin.citiesapi.controller.request.LoginUserRequest;
import hr.sanjin.citiesapi.service.dto.LoginTokenDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@Transactional
@AutoConfigureMockMvc
@ActiveProfiles("integration-test")
public abstract class AbstractMockMVCIntegrationTest {

	private static final String PATH_LOGIN = "/api/public/users/login";
	private static final String TEST_USER = "TestUser1";
	private static final String TEST_PASS = "pass";

	@Autowired
	protected MockMvc mockMvc;

	@Autowired
	protected ObjectMapper objectMapper;

	@Value("${cts.token.header.name}")
	private String authHeaderName;

	protected MockHttpServletRequestBuilder postWithoutAuth(String path, Object body) throws JsonProcessingException {
		return post(path)//
				.contentType(MediaType.APPLICATION_JSON)//
				.content(this.objectMapper.writeValueAsString(body));
	}

	protected MockHttpServletRequestBuilder postWithAuth(String path, Object body) throws Exception {
		String token = obtainToken();

		return post(path)//
				.contentType(MediaType.APPLICATION_JSON)//
				.header(this.authHeaderName, token)//
				.content(this.objectMapper.writeValueAsString(body));
	}

	protected MockHttpServletRequestBuilder deleteWithoutAuth(String path, Object body) throws JsonProcessingException {
		return delete(path)//
				.contentType(MediaType.APPLICATION_JSON)//
				.content(this.objectMapper.writeValueAsString(body));
	}

	protected MockHttpServletRequestBuilder deleteWithAuth(String path, Object body) throws Exception {
		String token = obtainToken();

		return delete(path)//
				.contentType(MediaType.APPLICATION_JSON)//
				.header(this.authHeaderName, token)//
				.content(this.objectMapper.writeValueAsString(body));
	}

	private String obtainToken() throws Exception {
		LoginUserRequest loginRequest = new LoginUserRequest();
		loginRequest.setUsername(TEST_USER);
		loginRequest.setPassword(TEST_PASS);

		MvcResult result = this.mockMvc //
				.perform(postWithoutAuth(PATH_LOGIN, loginRequest)) //
				.andExpect(status().isOk())//
				.andReturn();

		LoginTokenDTO tokenDTO = this.objectMapper.readValue(result.getResponse().getContentAsString(), LoginTokenDTO.class);

		return tokenDTO.getToken();
	}
}
