## Starting Application

### 1. Build with Maven

	./mvnw clean install
	
### 2. Execute jar

	java -jar target/cities-api-0.0.1-SNAPSHOT.jar
	
## Technology Used
- Spring
- H2 database
- Liquibase
- Swagger


## Usage

1. When application is started, navigate to http://localhost:8080/swagger-ui.html
2. Swagger UI opens with list of available Controllers
3. UserController and City Controller are available to all users
4. CityProtectedController is available only to users with valid token
5. To obtain token, first register user
6. Login with user credentials, and token is returned
7. In top right corner in Swagger, click on 'Authorize' button and paste token
8. Now you are able to call protected endpoints also

## Notes

- Application uses H2 database. Data is saved in data.mv.db and data.trace.db files
- If database files do not exist, they will be created on application startup
- Database is pre-populated with 10 city entries using Liquibase migration tool